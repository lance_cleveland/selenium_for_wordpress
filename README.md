# Selenium on JavaScript / WordPress (SoJa / WP)

A basic library to help start building automated user experience testing for WordPress sites.   

## Installation

### NodeJs

You will need NodeJS installed on your OS.

### Selenium

Once you have NodeJS you will need Selenium.

Selenium may be installed via npm with

    npm install selenium-webdriver

You will need to download additional components to work with each of the major
browsers.  

The drivers for Chrome, Firefox, PhantomJS, Opera, and Microsoft's IE and Edge web browsers are all standalone executables that should be placed on your system [PATH].  

Apple's safaridriver is shipped with Safari 10 for OS X El Capitan and macOS Sierra. You will need to enable Remote Automation in the Develop menu of Safari 10 before testing.

## Setup

Put this repository on your system then edit the environment files.

The ./env/*-example.js file contains and EXAMPLE of the basic settings used in the scripts.

You will need to copy the -example.js files to a -real.js file name to use this library in the production, staging, or development configuration.   The default environment is development.      

To start using the scripts to test your development server copy ./env/development-example.js to ./env/development-real.js and edit the file.  Set your WordPress admin username, password, email as well as the URL and title for the site. 

    module.exports = {
        user: 'admin',
        pwd: 'password',
        email: 'me@mydomain.com',
        url: 'http://wpslp.dev',
        title: 'SLP Dev',
    };
    
### Configuration Tweaks

You can further tweak the configuration setting things like the default browser (set to Safari), the default window size (1200x800), and the default window starting position (100,100).

These settings are found in the ./config/config.js file which has various sections defined on what you probably want to change, may want to change, and shouldn't really change.




## Usage

The default environment is the development environment.    This can be changed without editing code by setting the NODE_ENV environment variable before running your node script.

For example, to run the login script against the staging server instead copy staging-example.js to staging-real.js in the env directory then run this command from the OS command line:

    $ NODE_ENV=staging node login.js

## Included Scripts

The test scripts are included in the ./tests/ directory.

### Activate Site

The ./tests/activate_site.js script is used to setup the first user, the admin user, on the WordPress site after the database connection has been configured.

We use this on our Store Locator Plus development boxes every time we clear out the WordPress database to start a "fresh install" test cycle with a pre-configured WordPress install.

### Site Login

The ./tests/login.js will login into the site as an admin user.
 
## Documentation

None really though you may find some helpful articles on [Lance.bio][lancebioselenium]

## Contributing

Contributions are accepted either through [BitBucket][bitbucketsojawp] pull requests.

## License

[GNU General Public License v3 (GPL3)][gpl3]


[lancebioselenium]: https://github.com/nodejs/LTS
[bitbucketsojawp]: https://bitbucket.org/lance_cleveland/selenium_for_wordpress
[gpl3]: https://www.gnu.org/licenses/gpl-3.0.en.html