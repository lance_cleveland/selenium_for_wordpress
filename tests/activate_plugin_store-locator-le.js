// Load our common test lib
// Configure our default wait to quick_wait (1/2 second)
var test = test || require( '../lib/common' );
test.config.current_wait = test.config.setting.quick_wait;

// Login
require( './login.js' );

// Activate SLP
test.log.info( 'Activate plugin store-locator-le' );
test.driver.get( test.config.env.url + '/wp-admin/plugins.php' )
    .then( test.when_aria_label_exists_click( 'Activate Store Locator Plus' ) )
    .then( test.driver.sleep( test.config.current_wait ) )
    ;